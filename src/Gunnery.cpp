#include <Arduino.h>
#include <SPI.h>
#include <Adafruit_Soundboard.h>

const byte startButton = 2;
const byte fireButton = 3; // interrupt
#define xInput A0
#define yInput A1
const byte clockSwitch = 21; // interrupt
const byte stepEnb = 8;
const byte stepDir = 9;
const byte stepSpeed = 10; // use tone for speed
const int clockSpeed = 31;

#define diagLed 13

int xPos = 0;
int yPos = 0;
int xPosOld = 0;
int yPosOld = 0;

const byte sfxReset = 4;
const byte sfxActive = 5; // LOW when a track is playing

Adafruit_Soundboard sfx = Adafruit_Soundboard(&Serial1, NULL, sfxReset);
String sfxTracks[3][14] = {
    {"T1000001OGG", "T1000002OGG", "T1000003OGG", "T1000004OGG", "T1000005OGG",
     "T1000006OGG", "T1000007OGG", "T1000008OGG", "T1000009OGG", "T1000010OGG",
     "T1000011OGG", "T1000012OGG", "T1000013OGG", "T1000014OGG"},
    {"T2000001OGG", "T2000002OGG", "T2000003OGG", "T2000004OGG", "T2000005OGG",
     "T2000006OGG", "T2000007OGG", "T2000008OGG", "T2000009OGG", "T2000010OGG",
     "T2000011OGG", "T2000012OGG", "T2000013OGG", "T2000014OGG"},
    {"T3000001OGG", "T3000002OGG", "T3000003OGG", "T3000004OGG", "T3000005OGG",
     "T3000006OGG", "T3000007OGG", "T3000008OGG", "T3000009OGG", "T3000010OGG",
     "T3000011OGG", "T3000012OGG", "T3000013OGG", "T3000014OGG"}};

String iTracks[2] = {"FAIL0000OGG", "FIRE0000OGG"};

const byte led[3][3] = {{22, 24, 26}, {28, 30, 32}, {34, 36, 38}};

const int ledXY[3][3][2] = {{{158, 170}, {229, 251}, {260, 194}},
                            {{381, 358}, {460, 397}, {439, 370}},
                            {{574, 220}, {513, 97}, {546, 165}}};
const int deadZone = 4;
bool posGood = false;
bool timeUp = true;
bool fail = false;
long resetTimer = 0;
long resetTime = 20000;
byte gameID = 0; // game ID 0 to 2;

void clockISR() {  
  timeUp = true;
}

void setup() {
  
  Serial.begin(9600);
  Serial1.begin(9600); // sound fx board
  pinMode(diagLed, OUTPUT);
  for (int i = 0; i < 3; i++) {
    pinMode(led[i][0], OUTPUT); // led pins as outputs
    pinMode(led[i][1], OUTPUT);
    pinMode(led[i][2], OUTPUT);
    digitalWrite(led[i][0], LOW); // off
    digitalWrite(led[i][1], LOW);
    digitalWrite(led[i][2], LOW);
  }
  pinMode(startButton, INPUT_PULLUP);
  pinMode(fireButton, INPUT_PULLUP);
  pinMode(sfxActive, INPUT);
  pinMode(clockSwitch, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(clockSwitch), clockISR, FALLING);

  pinMode(stepEnb, OUTPUT);
  pinMode(stepDir, OUTPUT);
  digitalWrite(stepEnb, LOW);
  digitalWrite(stepDir, LOW);

  if (!sfx.reset()) {
    Serial.println("Not found");
    while (1)
      ;
  }
  Serial.println("SFX board found");
  delay(200);
  digitalWrite(stepEnb, HIGH);
  noTone(stepSpeed);
  for (int i = 0; i < 3; i++) {
    digitalWrite(led[i][0], HIGH); // off
    digitalWrite(led[i][1], HIGH);
    digitalWrite(led[i][2], HIGH);
    delay(2000);
    digitalWrite(led[i][0], LOW); // off
    digitalWrite(led[i][1], LOW);
    digitalWrite(led[i][2], LOW);
  }
  digitalWrite(diagLed, LOW);
}
void playSound(int gameNum, int trackNum) {
  char trackName[20];
  memset(trackName, 0, sizeof(trackName));
  sfxTracks[gameNum][trackNum].toCharArray(trackName, 20);
  sfx.playTrack(trackName);
  delay(500);
  bool Act = digitalRead(sfxActive);
  while (!Act) {
    Act = digitalRead(sfxActive);
  }
}
void playIncidentalSound(int snd) {
  char trackName[20];
  iTracks[snd].toCharArray(trackName, 20);
  sfx.playTrack(trackName);
  delay(500);
  bool Act = digitalRead(sfxActive);
  while (!Act) {
    Act = digitalRead(sfxActive);
  }
}
void getPosition(int gameNum, int ledNum) {
  posGood = false;
  xPos = analogRead(xInput);
  if (xPos >= (ledXY[gameNum][ledNum][0] - deadZone) &&
      xPos <= (ledXY[gameNum][ledNum][0] + deadZone)) {
    yPos = analogRead(yInput);
    if (yPos >= (ledXY[gameNum][ledNum][1] - deadZone) &&
        yPos <= (ledXY[gameNum][ledNum][1] + deadZone)) {
      posGood = true;
    }
  }
}
void clockStart() {
  digitalWrite(stepDir, HIGH);
  digitalWrite(stepEnb, LOW);
  tone(stepSpeed, clockSpeed);
  delay(2000); // drive off magnet delay
  timeUp = false;
}
void clockStop() {
  delay(1500);
  digitalWrite(stepEnb, HIGH);
  noTone(stepSpeed);
}
void ledsOff() {
  for (int i = 0; i < 3; i++) {
    digitalWrite(led[i][0], LOW); // off
    digitalWrite(led[i][1], LOW);
    digitalWrite(led[i][2], LOW);
  }
}
void loop() {

  if (digitalRead(clockSwitch)) {   // reset clock..
    digitalWrite(stepDir, HIGH);
    digitalWrite(stepEnb, LOW);
    tone(stepSpeed, 500);
  }
  while (digitalRead(clockSwitch)) {
  }
  delay(100);
  digitalWrite(stepEnb, HIGH);
  noTone(stepSpeed);

  bool run = digitalRead(startButton);
  posGood = false;
  run = true;
  timeUp = false;
  fail = false;
  while (run) {
    run = digitalRead(startButton);
  }
  clockStart();
  // ...part 1 of 3....................................................................

  playSound(gameID, 0); // adjustment (radio)
  delay(500);
  playSound(gameID, 1); // co-ordinates
  resetTimer = millis();
  timeUp = false;
  digitalWrite(diagLed, LOW);
  Serial.println("001");                        //**************
  while (!run && !timeUp) {
    getPosition(gameID, 0);
    delay(50);
    if (millis() > resetTimer + resetTime) {
      timeUp = true;
    }
    if (posGood) {
      digitalWrite(led[gameID][0], HIGH);
      run = true; // will break us ot of the while
    }
  }
  Serial.println("002");
  if (timeUp) {
    fail = true;
    goto failed;
  }
  playSound(0, 2); // radio fire
  playSound(0, 3); // local fire
  while (digitalRead(fireButton) && !timeUp) {
  }
  if (timeUp) {
    fail = true;
    goto failed;
  }
  playIncidentalSound(1); //
  digitalWrite(led[gameID][0], LOW);

  // ...part 2 of 3...
  playSound(gameID, 4); // adjustment (radio)
  delay(50);
  playSound(gameID, 5); // co-ordinates
  run = false;
  resetTimer = millis();
  delay(50);
  while (!run && !timeUp) {
    getPosition(gameID, 1);
    delay(50);
    if (millis() > resetTimer + resetTime) {
      timeUp = true;
    }
    if (posGood) {
      digitalWrite(led[gameID][1], HIGH);
      run = true; // will break us ot of the while
    }
  }
  if (timeUp) {
    fail = true;
    goto failed;
    }
  playSound(0, 6); // radio fire
  playSound(0, 7); // local fire
  while (digitalRead(fireButton) && !timeUp) {
  }
  if (timeUp) {
    fail = true;
    goto failed;
  }
  playIncidentalSound(1); //
  digitalWrite(led[gameID][1], LOW);
  // ...part 3 of 3...
  playSound(gameID, 8); // adjustment (radio)
  delay(50);
  playSound(gameID, 9); // co-ordinates
  run = false;
  delay(50);
  while (!run && !timeUp) {
    getPosition(gameID, 2);
    delay(50);
    if (posGood) {
      digitalWrite(led[gameID][2], HIGH);
      run = true; // will break us ot of the while
    }
  }
  if (timeUp) {
    fail = true;
    goto failed;
  }
  playSound(0, 10); // radio fire
  playSound(0, 11); // local fire
  while (digitalRead(fireButton) && !timeUp) {
  }
  if (timeUp) {
    fail = true;
    goto failed;
  }
  playIncidentalSound(1); //
  digitalWrite(led[gameID][2], LOW);
  playSound(0, 12); // hit
  playSound(0, 13); // good shooting

failed:
  if (fail) {
    clockStop();
    Serial.println("Failed..");
    playIncidentalSound(0);
  }
  ledsOff();
  gameID = gameID + 1;
  if (gameID == 3) {
    gameID = 0;
  }
}
